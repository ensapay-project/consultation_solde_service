package com.ensapay.demo.enumerations;

public enum Role {

    ADMIN,
    AGENT,
    CLIENT
}
