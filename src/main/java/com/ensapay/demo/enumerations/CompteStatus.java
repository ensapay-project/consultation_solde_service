package com.ensapay.demo.enumerations;

public enum CompteStatus {

    ACTIVE,
    BLOCKED
}
