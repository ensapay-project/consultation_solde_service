package com.ensapay.demo.repositories;

import com.ensapay.demo.models.Solde;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.webmvc.RepositoryRestController;

import java.time.LocalDate;
import java.util.List;

@RepositoryRestController
public interface SoldeRepository extends MongoRepository<Solde, Long> {

    List<Solde> findAllByNumeroCompte(String numeroCompte);

    List<Solde> findAllByNumeroCompteAndSolde(String numeroCompte, double solde);

    List<Solde> findAllByNumeroCompteAndDate(String numeroCompte, LocalDate date);

}
