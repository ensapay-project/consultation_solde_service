package com.ensapay.demo;

import com.ensapay.demo.enumerations.Role;
import com.ensapay.demo.models.Client;
import com.ensapay.demo.models.Compte;
import com.ensapay.demo.models.User;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class DemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }


    public static Compte justForTest() {
        User user = User.builder()
                .email("Khalil@gmail.com")
                .nom("khalil")
                .password("khalil")
                .adresse("Marrakech")
                .role(Role.CLIENT)
                .build();
        Client client = Client.builder().user(user).build();
        return Compte.builder()
                .numeroCompte("123456789")
                .solde(1500.23)
                .client(client)
                .intitule("khalil")
                .build();
    }


}
