package com.ensapay.demo.communications;


import com.ensapay.demo.models.Agent;
import com.ensapay.demo.models.Compte;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Optional;

@FeignClient(name="compte-service",url = "http://localhost:8090/compte/api",configuration = Compte.class)

public interface CompteRequest {

    @GetMapping("/compte/api/compte/{numeroCompte}")
    Optional<Compte> getSolde(@PathVariable String numeroCompte);

    @PostMapping("/compte/api/compte/{numeroCompte}/addsolde")
    Optional<Compte> setSolde(@PathVariable String numeroCompte, @RequestParam double solde);


}
