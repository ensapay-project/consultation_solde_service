package com.ensapay.demo.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonUnwrapped;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;

import java.util.Collection;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Client {

    @Id
    private Long id;

    //    @OneToOne
    @JsonIgnoreProperties({"admin", "dateUpdate", "dateDeCreation", "id"})
    @JsonUnwrapped
    private User user;

    //    @OneToMany(mappedBy = "client",fetch = FetchType.LAZY,  cascade={CascadeType.REMOVE})
    @JsonIgnoreProperties({"client"})
    private Collection<Compte> comptes;

}
