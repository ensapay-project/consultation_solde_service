package com.ensapay.demo.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonUnwrapped;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;


@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Agent {

    @Id
    private Long id;

    //    @OneToOne
    @JsonIgnoreProperties({"agent", "dateUpdate", "dateDeCreation", "id"})
    @JsonUnwrapped
    private User user;
}
