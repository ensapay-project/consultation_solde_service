package com.ensapay.demo.controllers;

import com.ensapay.demo.communications.CompteRequest;
import com.ensapay.demo.enumerations.CompteStatus;
import com.ensapay.demo.models.Compte;
import com.ensapay.demo.models.Solde;
import com.ensapay.demo.repositories.SoldeRepository;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/solde/api")
@Api(description = "API pour les opérations CRUD sur l'entité solde")
@RequiredArgsConstructor
@Log4j2
public class SoldeController {

    public CompteRequest compteRequest;
    private final SoldeRepository soldeRepository;


    /**
     * This Api is for getting all the histories in DB by soldes or modification date
     *
     * @param numeroCompte
     * @param solde
     * @return List of soldes
     */
    @GetMapping("/{numeroCompte}/search-history")
    @ApiOperation(value = "Trouver l'historiques à partir de solde ou operateur ou date de modification (consultation)")
    public ResponseEntity<List<Solde>> getHistory(@PathVariable String numeroCompte,
                                                  @RequestBody Solde solde) {
        List<Solde> soldeList = new ArrayList<>();
        if (solde.getDate() != null) soldeList.addAll(soldeRepository
                .findAllByNumeroCompteAndDate(numeroCompte, solde.getDate()));
        if (solde.getSolde() != 0) soldeList.addAll(soldeRepository
                .findAllByNumeroCompteAndSolde(numeroCompte, solde.getSolde()));
        return ResponseEntity.ok(soldeList);
    }

    /**
     * This api is for getting all history of an account
     *
     * @param numeroCompte
     * @return solde entity
     */
    @GetMapping("/{numeroCompte}/all-histories")
    @ApiOperation(value = "Récupérer tous l'historiques d'un compte à partir de son numéro")
    public ResponseEntity<List<Solde>> getAllHistory(@PathVariable String numeroCompte) {
        return ResponseEntity.ok(soldeRepository.findAllByNumeroCompte(numeroCompte));
    }

    /**
     * This api is for getting the balance from any Account
     *
     * @param numeroCompte
     * @return total solde account
     */

    @GetMapping("/{numeroCompte}")
    @ApiOperation(value = "Récupérer le solde d'un compte à partir de son numéro")
    public Compte getSolde(@PathVariable String numeroCompte) {
        return  Compte.builder()
                .solde(1500)
                .numeroCompte("123456")
                .intitule("Rose")
                .statut(CompteStatus.ACTIVE)
                .build();
//        Compte compte = compteRequest.getSolde(numeroCompte).orElseThrow(
//                () -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Compte introuvable!")
//        );
//        return compte;
    }

    /**
     * This api is for adding balance to account
     *
     * @param numeroCompte
     * @param solde (object)
     * @return new solde
     */

    @PostMapping("/{numeroCompte}/add-solde")
    @ApiOperation(value = "Ajouter le solde à un compte")
    public ResponseEntity<Solde> addSolde(@PathVariable String numeroCompte,
                                          @RequestBody Solde solde) {

        Compte compte = getSolde(numeroCompte);

//        Compte compte = compteRequest.getSolde(numeroCompte).orElseThrow(
//                () -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Compte introuvable!"));

        double total = compte.getSolde() + solde.getSolde();
        if (total < 0) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Votre solde est insuffisant !");
            }

        Solde newSolde = Solde.builder()
                .numeroCompte(compte.getNumeroCompte())
                .solde(solde.getSolde())
                .date(LocalDate.now())
                .build();
        soldeRepository.save(newSolde);
        log.info(total);
//        compteRequest.setSolde(numeroCompte, total).orElseThrow(
//                () -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "Vous ne pouvez pas ajouter de solde!")
//        );

        return ResponseEntity.ok(newSolde);

    }


    /**
     * This api is for making solde = 0
     *
     * @param numeroCompte
     * @return put solde = 0
     */
    @PutMapping("/{numeroCompte}/remove-solde")
    @ApiOperation(value = "supp le solde dans un compte")
    public ResponseEntity<Solde> soldeToZero(@RequestParam String numeroCompte) {

        Compte compte = compteRequest.getSolde(numeroCompte).orElseThrow(
                () -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Compte introuvable!")
        );

        Solde solde = Solde.builder()
                .solde(compte.getSolde())
                .date(LocalDate.now())
                .build();
        soldeRepository.save(solde);

        compteRequest.setSolde(numeroCompte, 0).orElseThrow(
                () -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "Vous ne pouvez pas ajouter de solde!")
        );

        return ResponseEntity.ok(solde);
    }

}
